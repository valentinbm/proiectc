#include "Game.h"
#include "Buttons.h"


void Game::action()
{
}

Game::Game()
{
	Buttons *sp,*mp,*qb;
	sf::RenderWindow window(sf::VideoMode(400, 400), "SFML works!");
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Green);
	sf::Font ft;
	ft.loadFromFile("arial.tTf");
	sp = new Buttons(50, 50, 200, 50, ft, "Singal Player", 30,
		sf::Color(0, 200, 0), sf::Color(0, 227, 0), sf::Color(0, 255, 0)
		, sf::Color(127, 0, 127), sf::Color(227, 0, 227), sf::Color(255, 0, 255)
		, sf::Color(0, 127, 127), sf::Color(0, 227, 227), sf::Color(0, 255, 255)
	);
	mp = new Buttons(50, 100, 200, 50, ft, "Multy Player", 30,
		sf::Color(0, 200, 0), sf::Color(0, 227, 0), sf::Color(0, 255, 0)
		, sf::Color(127, 0, 127), sf::Color(227, 0, 227), sf::Color(255, 0, 255)
		, sf::Color(0, 127, 127), sf::Color(0, 227, 227), sf::Color(0, 255, 255)
	);
	qb = new Buttons(50, 150, 200, 50, ft, "Quit", 30,
		sf::Color(0, 200, 0), sf::Color(0, 227, 0), sf::Color(0, 255, 0)
		, sf::Color(127, 0, 127), sf::Color(227, 0, 227), sf::Color(255, 0, 255)
		, sf::Color(0, 127, 127), sf::Color(0, 227, 227), sf::Color(0, 255, 255)
	);
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		sp->update(sf::Mouse::getPosition(window));
		mp->update(sf::Mouse::getPosition(window));
		qb->update(sf::Mouse::getPosition(window));
		if (qb->action())
		{
			window.close();
			delete sp,mp, qb;
			return;
		}
		window.clear();
		window.draw(shape);
		sp->render(window);
		mp->render(window);
		qb->render(window);
		window.display();
	}

}


Game::~Game()
{
}

void Game::UpdateSFMLEvents()
{
}

void Game::Update()
{
}

void Game::Render()
{
}

void Game::Run()
{
	
}
