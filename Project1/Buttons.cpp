#include "Buttons.h"





Buttons::Buttons(float PozX, float PozY, float Width,
	float Height, sf::Font & Font, const  std::string &Text,
	unsigned int Size,
	sf::Color textIdleColor,
	sf::Color textHoverColor,
	sf::Color textActiveColor,
	sf::Color bgIdleColor,
	sf::Color bgHoverColor,
	sf::Color bgActiveColor,
	sf::Color borderIdleColor = sf::Color::Transparent,
	sf::Color borderHoverColor = sf::Color::Transparent,
	sf::Color borderActiveColor = sf::Color::Transparent
	
):textActiveColor(textActiveColor),textHoverColor(textHoverColor),textIdleColor(textIdleColor),
bgActiveColor(bgActiveColor), bgHoverColor(bgHoverColor), bgIdleColor(bgIdleColor),
borderActiveColor(borderActiveColor), borderHoverColor(borderHoverColor), borderIdleColor(borderIdleColor),
Font(Font)
{
	//this->action = action;
	this->shape.setPosition(sf::Vector2f(PozX, PozY));
	this->shape.setSize(sf::Vector2f(Width, Height));
	this->shape.setFillColor(bgIdleColor);
	this->shape.setOutlineThickness(1.f);
	this->shape.setOutlineColor(borderIdleColor);


	this->text.setFont(this->Font);
	this->text.setCharacterSize(Size);
	this->text.setColor(this->textIdleColor);
	this->text.setString(sf::String(Text));

	this->text.setPosition(
		this->shape.getPosition().x + (this->shape.getGlobalBounds().width / 2.f) - (this->text.getGlobalBounds().width / 2.f),
		this->shape.getPosition().y 
	);
}

void Buttons::update(sf::Vector2i  mousePoz)
{
	int8_t status = 0;
	if (this->shape.getGlobalBounds().contains(static_cast<sf::Vector2f>(mousePoz)))
	{
		status = 1;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			status = 2;
			triger = true;
		}

		
	}
	
	switch (status)
	{
	case 0:
		this->shape.setFillColor(this->bgIdleColor);
		this->text.setFillColor(this->textIdleColor);
		this->shape.setOutlineColor(this->borderIdleColor);
		break;

	case 1:
		this->shape.setFillColor(this->bgHoverColor);
		this->text.setFillColor(this->textHoverColor);
		this->shape.setOutlineColor(this->borderHoverColor);
		break;

	case 2:
		this->shape.setFillColor(this->bgActiveColor);
		this->text.setFillColor(this->textActiveColor);
		this->shape.setOutlineColor(this->borderActiveColor);
		break;

	}
}

void Buttons::render(sf::RenderTarget & targed)
{
	targed.draw(this->shape);
	targed.draw(this->text);
}


Buttons::~Buttons()
{
}
