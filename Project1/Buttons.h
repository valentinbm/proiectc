#pragma once
#include "Headers.h"
class Buttons
{
private:
	
	sf::Text text;
	sf::RectangleShape shape;
	sf::Font &Font;

	sf::Color textIdleColor;
	sf::Color textHoverColor;
	sf::Color textActiveColor;

	sf::Color bgIdleColor;
	sf::Color bgHoverColor;
	sf::Color bgActiveColor;

	sf::Color borderIdleColor;
	sf::Color borderHoverColor;
	sf::Color borderActiveColor;
	
	bool triger=false;
public:
	Buttons(float  PozX ,float PozY,float Width,float Height,
		sf::Font & Font ,
		const std::string &Text,
		unsigned int Size ,
		sf::Color textIdleColor ,
		sf::Color textHoverColor ,
		sf::Color textActiveColor,

		sf::Color bgIdleColor,
		sf::Color bgHoverColor,
		sf::Color bgActiveColor,

		sf::Color borderIdleColor,
		sf::Color borderHoverColor,
		sf::Color borderActiveColor
);
	void update(sf::Vector2i mousePoz);
	void render(sf::RenderTarget&targed);
	virtual ~Buttons();
	bool action()
	{
		return triger;
		
	}
};

