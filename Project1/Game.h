#pragma once
#include <SFML/Graphics.hpp>
class Game
{
private:
	sf::RenderWindow *Window;
	sf::Event sfEvent;
	void action();
public:
	Game();
	virtual ~Game();
	void UpdateSFMLEvents();
	void Update();
	void Render();
	void Run();
};

