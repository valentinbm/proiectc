#pragma once
#include<SFML/Graphics.hpp>
class nod
{
public:
	nod(sf::Vector2f poz,int name);
	nod(const nod&nd)
	{
		ft = nd.ft;
		poz = nd.poz;
		Shape = nd.Shape;
		NodeText = nd.NodeText;
		name = nd.name;
		raza = nd.raza;
	}
	~nod();

	
	
private:
	sf::Font ft;
	sf::Vector2f poz;
	sf::CircleShape Shape;
	sf::Text NodeText;
	int name;
	int raza = 10;
//function
public :
	bool IsOver(const sf::Vector2i&poz);
	bool IsOver(nod &Compare);
	void Draw(sf::RenderWindow & window);
	sf::Vector2f GetPoz() const {
		return poz;
	};
	void SetPoz(const sf::Vector2i& poz);
};

