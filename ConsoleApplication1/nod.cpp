#include "pch.h"
#include "nod.h"
#include<iostream>


nod::nod(sf::Vector2f poz,int name)
{
	this->poz = poz;
	
	ft.loadFromFile("./arial.ttf");
	NodeText.setFont(ft);
	NodeText.setString(std::to_string(name));
	NodeText.setOrigin(NodeText.getLocalBounds().height/2, NodeText.getLocalBounds().width / 2);
	NodeText.setFillColor(sf::Color(0, 0, 0));
	NodeText.setCharacterSize(15);
	Shape.setOrigin(raza, raza);
	Shape.setRadius(raza);
	Shape.setPosition(this->poz);
	NodeText.setPosition(this->poz);
	this->name = name;
}

nod::~nod()
{
	this->GetPoz();
}



bool nod::IsOver(const sf::Vector2i&poz)
{
	if ((this->poz.x - poz.x + this->poz.y - poz.y)*(this->poz.x - poz.x + this->poz.y - poz.y) <=
 raza*raza  )
		return true;

	return false;
}

bool nod::IsOver(nod & Compare)
{
	if (this == &Compare)
		return false;
	if ((this->poz.x-Compare.GetPoz().x +
		this->poz.y-Compare.GetPoz().y ) *
		(this->poz.x - Compare.GetPoz().x +
			this->poz.y - Compare.GetPoz().y) <= (this->raza * Compare.raza))
		return true;
	return false;
}

void nod::Draw(sf::RenderWindow & window)
{
	window.draw(Shape);
	const sf::Font *s=NodeText.getFont();
	NodeText.setFont(ft);
	window.draw(NodeText);
}

void nod::SetPoz(const sf::Vector2i & poz)
{
	this->poz = static_cast<sf::Vector2f>(poz);
	Shape.setPosition(this->poz);
	NodeText.setPosition(this->poz);
	
}
