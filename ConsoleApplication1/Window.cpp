#include "pch.h"
#include "Window.h"
#include <SFML/Graphics.hpp>
#ifdef _DEBUG

#pragma comment(lib,"lib/sfml-audio-d.lib")
#pragma comment(lib,"lib/sfml-graphics-d.lib")
#pragma comment(lib,"lib/sfml-network-d.lib")
#pragma comment(lib,"lib/sfml-system-d.lib")
#pragma comment(lib,"lib/sfml-window-d.lib")
#endif
#ifdef NDEBUG
#pragma comment(lib,"lib/sfml-audio.lib")
#pragma comment(lib,"lib/sfml-graphics.lib")
#pragma comment(lib,"lib/sfml-network.lib")
#pragma comment(lib,"lib/sfml-system.lib")
#pragma comment(lib,"lib/sfml-window.lib")
#endif

Window::Window():window(sf::VideoMode(800, 600), "SFML window")
{
	
}


Window::~Window()
{
}

void Window::mainLoop()
{
	sf::Event evt;
	sf::Event::EventType evtTip=sf::Event::EventType::Closed;
	int NodCount = 0;
	while (window.isOpen())
	{
		bool change = false;
		window.pollEvent(evt);
		if (evt.type != evtTip)
		{
			change = true;
			evtTip = evt.type;
		}
		switch (evt.type)
		{
		case sf::Event::Closed:
				window.close();
				break;
		case sf::Event::MouseButtonReleased:
			if(change){
			bool draw = true;
				

				for (nod nodCurent : listaNoduri)
					if (nodCurent.IsOver(sf::Vector2i(evt.mouseButton.x,evt.mouseButton.y)))
					{

						draw = false;
						
					}
				if (draw)
				listaNoduri.emplace_back(sf::Vector2f(evt.mouseButton.x, evt.mouseButton.y), NodCount++);
			}
			break;
		}
		window.clear();
		for (nod nodCurent : listaNoduri)
			nodCurent.Draw(window);
		window.display();
	}
}
