#pragma once
#include "nod.h"
#include <vector>
class Window
{
public:
	Window();
	~Window();
	void mainLoop();
private:
	sf::RenderWindow window;
	std::vector<nod>listaNoduri;
};

